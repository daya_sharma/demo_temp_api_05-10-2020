

module.exports = {

    // create database connection 
    dbmodule: (query, cb) => {
	
        const { Client } = require('pg')        // create pgsql client 
    
        // configure the database
        const client = new Client({
            host: 'localhost',
            port: 5432,
            user: 'postgres',
            password: '',
            database: 'demo',
    
        })
    
    
        client.connect((err) => {
    
            if (err) { console.log('API to PGSQL connection Problem') 
            console.log(err.stack)
            
            }
    
            else {
                console.log('AWS Database Connected .......');
                client.query(query, (err, res) => {
                    if (err) { cb(err, null)
                    }
                    else cb(null, res)
                    client.end();	
                })
    
              }
    
        })
    
    },


}