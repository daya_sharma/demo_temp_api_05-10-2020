// install express package 
var express=require('express');
var app=express();
//assign middleware file location 
var middleware=require('./middleware/middleware');

//use that middleware file
app.use(middleware);

// Assign the server listening port 
app.listen(3000,()=>{
console.log('listening to port 3000');
})