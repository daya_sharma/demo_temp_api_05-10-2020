var express=require('express');
var app=express();      // create a common variable for express

// locate api file where each api is located 
var api=require('../controller/api');

// call the api function
app.get('/getUserDetails/:id?/:parent_id?', function (req, res) {
  api.getUserDetails(req, res)
});

// call the api function
app.get('/getDeparmentDetails/:id?', function (req, res) {
  api.getDeparmentDetails(req, res)
});

// searchDetails
app.get('/searchDetails/:dept_name?/:emp_name?', function (req, res) {
  api.searchDetails(req, res)
});

// deleteRecords
app.post('/deleteRecords', function (req, res) {
  api.deleteRecords(req, res)
});

// addORUpdateRecords
app.post('/addORUpdateRecords', function (req, res) {
  api.addORUpdateRecords(req, res)
});


//define dependencies and modules  (npm ecosystem) 
module.exports=app